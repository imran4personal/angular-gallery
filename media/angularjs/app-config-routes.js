var app = angular.module('laravelGallery', ['ngRoute','jkuri.gallery']);
var app_url = "http://localhost:8000/";
var app_avatar_url = app_url+'uploads/gallery/';
var app_gallery_url = app_url+'uploads/user_avatar/';
var local_app_url = 'http://localhost/angular_gallery/';

app.controller('loadConstants', function($scope) {
    $scope.master = {
                        gallery_image_path:app_url+'uploads/gallery/',
                        user_image_path:app_url+'uploads/user_avatar/',
                        local_app_url:'http://localhost/angular_gallery/',
                        app_backend__url:'http://localhost:8000/'
                    };
});    

app.config(function ($routeProvider) {
    $routeProvider
        .when('/',
                {
                    controller:'galleryCtrl',
                    templateUrl: 'pages/gallery.html',
                    method:'loadGallery'
                })
        .when('/home',
                {
                    controller:'galleryCtrl',
                    templateUrl: 'pages/gallery.html',
                    method:'loadGallery'
                })
        .when('/sign_in',
                {
                    controller:'loginCtrl',
                    templateUrl: 'pages/login.html'
                })
        .when('/signout',
                {
                    controller:'loginCtrl',
                    templateUrl: 'pages/login.html',
                    method:'logOutUser'
                })
        .when('/sign_up',
                {
                   controller:'signupCtrl',
                    templateUrl: 'pages/sign_up.html'
                })
        .when('/forgot_password',
                {
                   templateUrl: 'pages/forgot_password.html'
                })
        .when('/detail/:image_id',
                {
                    controller: 'galleryCtrl',
                    templateUrl: 'pages/image_detail.html',
                    method:"imageDetail"
                })
        .when('/upload_image',
                {
                    controller: 'ManageGallery',
                    templateUrl: 'pages/upload_image.html',
                    method:"loadUserDetail"
                })
        .when('/edit_image/:image_id',
                {
                    controller: 'ManageGallery',
                    templateUrl: 'pages/upload_image.html',
                    method:"loadUserDetail"
                })
        .when('/dashboard',
                {
                    controller: 'dashboard',
                    templateUrl: 'pages/dashboard.html',
                    method:'loadDashboardInfo'
                })
        .when('/manage_profile',
                {
                    controller: 'manageProfile',
                    templateUrl: 'pages/manage_profile.html'
                })
        .when('/change_password',
                {
                    controller: 'changePassword',
                    templateUrl: 'pages/change_password.html',
                    method:'loadUserDetail'
                })
        .otherwise({redirect: '/home'});
});