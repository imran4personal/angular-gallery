app.controller('dashboard', function($scope, $http, $location, $rootScope, $route) {
    if(localStorage.user_gallery_token === undefined){
        $location.url("/sign_in");
    }
    
    $scope.gallery_image_delete_info={
                image_id:undefined,
                index_id:undefined
            }
    $scope.loadDashboardInfo=function(){
        $http({
            url:app_url+"member_gallery",
            method:"POST",
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' },
            data: "token=" + localStorage.user_gallery_token,
        }).success(function (response) {
            $scope.gallery_images = response.gallery;
            $scope.owner = response.user;
            if(response.user != ''){
                if(response.user[0].avatar == null || response.user[0].avatar == ''){
                    $scope.user_avatar=app_gallery_url+"default_user_img.jpg";
                }else{
                    $scope.user_avatar=app_gallery_url+response.user[0].avatar;
                }
            }
        });
    }
    
    $scope.setValue=function(id,counter){
        $scope.gallery_image_delete_info.image_id=id;
        $scope.gallery_image_delete_info.index_id=counter;
    }
    
    $scope.deleteImage=function(){
        var image_id = $scope.gallery_image_delete_info.image_id;
        var counter = $scope.gallery_image_delete_info.index_id;
        
        $http({
            url:app_url+"gallery/delete_image",
            method:"POST",
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' },
            data: "image_id=" + image_id,
        }).success(function (response) {
            if(response.status === 'deleted'){
                $scope.gallery_images.splice(counter,1);
                $('#delete').modal('hide');
            }
        });
    }
    
    $scope.init=function(){
        if($route.current.method != undefined){
            $scope[$route.current.method]();
        }
    }
    
    $scope.init();
    
});