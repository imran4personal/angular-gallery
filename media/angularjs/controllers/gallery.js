app.controller('galleryCtrl', function($scope, $http, $route, $routeParams, $location) {
    
    
    $scope.loadGallery = function(){
        var page = getQueryString('page'); // returns 'chicken'
        
        if(page === null){
           page = 1; 
        }
        
        $http.get(app_url+"load_gallery_count").then(function(response) {
                       
            var input=[];
            var total = Math.ceil(response.data/12);
            total = parseInt(total);

            for (var i=0; i<total; i++) {
              input.push(i);
            }
            $scope.total_rec = input;
            
        });
        
        $http.get(app_url+"load_gallery?page="+page).then(function(response) {
            var galler_image=response.data;
            
            var imagesss = [];
            for (i = 0; i < galler_image.length; i++) {
                var item = galler_image[i];

                imagesss.push({ 
                     "thumb" : app_avatar_url+item.image,
                     "img"  : app_avatar_url+item.image,
                     "description" : item.title,
                     "detail_url" : '#/detail/'+item.id
                 });
            }
           
            $scope.images = imagesss;
            $scope.page = page;
                
            if(localStorage.user_gallery_token !== undefined){
                 $scope.uploadImageGallery=true;
            }
        });
    }
    
    $scope.imageDetail = function(){
        var image_id=$routeParams.image_id;
        
        $http.get(app_url+"load_image_detail/"+image_id).success(function(response) {
            $scope.detail = response;
        });
    }
    
       
    $scope.init=function(){
        if($route.current.method != undefined){
            $scope[$route.current.method]();
        }
    }
    
    $scope.init();
});

var getQueryString = function ( field, url ) {
    var href = url ? url : window.location.href;
    var reg = new RegExp( '[?&]' + field + '=([^&#]*)', 'i' );
    var string = reg.exec(href);
    return string ? string[1] : null;
};

