app.controller('loadUserInfo', function($scope, $http, $rootScope) {
    $scope.user_info=[];
    if(localStorage.user_gallery_token !== undefined){
        var token=localStorage.user_gallery_token;
        
        $http.get(app_url+"load_user_info/"+token).success(function(response) {
            $scope.user_info = response;
            $rootScope.afterLogin=true;
            $rootScope.beforeLogin=true;
            $rootScope.logedin_user_name = response[0].first_name+' '+response[0].last_name;
        });
    }
});