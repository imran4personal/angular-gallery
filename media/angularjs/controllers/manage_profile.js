app.controller('manageProfile', function($scope, $http, $location, $rootScope) {
    if(localStorage.user_gallery_token === undefined){
        $location.url("/sign_in");
    }
    var  token = localStorage.user_gallery_token;
    
    $http.get(app_url+"load_user_info/"+token).success(function(response) {
        $scope.profile_info={
                user_firstname:response[0].first_name,
                user_lastname:response[0].last_name,
                user_address:response[0].address,
                user_phone:response[0].contact,
                user_dob:response[0].d_b_o,
                user_bio:response[0].bio,
                user_email:response[0].email,
                user_name:response[0].user_name,
                user_profile_image:response[0].avatar
            }
    });
    
    $scope.updateProfile = function(valid){
        if(valid){
        var formdata = new FormData();
        
        formdata.append( 'token', localStorage.user_gallery_token );
        formdata.append( 'user_firstname', $scope.profile_info.user_firstname );
        formdata.append( 'user_lastname', $scope.profile_info.user_lastname );
        formdata.append( 'user_address', $scope.profile_info.user_address );
        formdata.append( 'user_phone', $scope.profile_info.user_phone );
        formdata.append( 'user_dob', $scope.profile_info.user_dob );
        formdata.append( 'user_bio', $scope.profile_info.user_bio );
        formdata.append( 'file', $scope.profile_info.user_profile_image );


        $http.post(app_url+'manage_profile',formdata,
                {
                    headers: {'Content-Type' : undefined}
                })
                .success(function(response){
                    $rootScope.logedin_user_name=$scope.profile_info.user_firstname+' '+$scope.profile_info.user_lastname;
                    if(response.avatar != '' && response.avatar != null){
                        $scope.profile_info.user_profile_image=response.avatar;
                    }
                    
                    $scope.profileUpdate=true;
                })
        }else{
            console.log("invalid form");
        }
    }

});