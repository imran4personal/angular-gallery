app.controller('signupCtrl', function($scope, $http, $location, $rootScope) {
    if(localStorage.user_gallery_token !== undefined){
        $location.url("/dashboard");
    }
    // valiables
    $scope.signup_info={
        first_name:undefined,
        last_name:undefined,
        display_name:undefined,
        user_password:undefined,
        user_email:undefined,
        password_confirmation:undefined
    }
    // function
    $scope.registerUser = function(valid){
        if(valid){
            if($scope.signup_info.user_password == $scope.signup_info.password_confirmation){
                $http({
                    url:app_url+"do_signup",
                    method:"POST",
                    headers : { 'Content-Type': 'application/x-www-form-urlencoded' },
                    data: "first_name=" + $scope.signup_info.first_name + "&last_name=" + $scope.signup_info.last_name + "&display_name=" + $scope.signup_info.display_name + "&user_password=" + $scope.signup_info.user_password + "&user_email=" + $scope.signup_info.user_email,
                }).success(function (response) {
                    if(response != ''){
                         localStorage.setItem("user_gallery_token",response);
                         $rootScope.logedin_user_name=$scope.signup_info.first_name+' '+$scope.signup_info.last_name;
                         $rootScope.afterLogin=true;
                         $rootScope.beforeLogin=true;
                         $location.url("/dashboard");
                     }else{
                         $scope.alreadyexistsuser=true;
                     }
                });
            }else{
                $scope.didnotmatch=true;
            } 
        }else{
            console.log("invalid form");
        }     
    }
});


