app.controller('loginCtrl', function($scope, $http, $location, $route, $rootScope, $window) {
    // valiables
    $scope.login_info={
        user_name:undefined,
        user_password:undefined
    }
    // function
    $scope.loginUser = function(valid){
        if(localStorage.user_gallery_token !== undefined){
            $location.url("/dashboard");
        }
        if(valid){
            $http({
                url:app_url+"get_authtication",
                method:"POST",
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' },
                data: "username=" + $scope.login_info.user_name + "&userpassword=" + $scope.login_info.user_password,
            }).success(function (response) {
                       if(response != ''){
                        localStorage.setItem("user_gallery_token",response.token);
                        $rootScope.logedin_user_name=response.user_name;
                        $rootScope.afterLogin=true;
                        $rootScope.beforeLogin=true;
                        $location.url("/dashboard");    

                    }else{
                        $scope.invalidUserName=true;
                    }
            });
        }else{
            console.log("invalid form");
        }
    }
    
    $scope.logOutUser = function(){
        $http({
            url:app_url+"log_out",
            method:"POST",
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' },
            data: "token=" + localStorage.user_gallery_token,
        }).success(function (response) {
                if(response.status == 'logout'){
                    localStorage.clear();
                    $rootScope.afterLogin=false;
                    $rootScope.beforeLogin=false;
                    $location.url("/home");    
                }
        });
    }
    
    $scope.init=function(){
        if($route.current.method != undefined){
            $scope[$route.current.method]();
        }
    }
    
    $scope.init();
    
    });