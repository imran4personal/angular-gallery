app.controller('ManageGallery', function($scope, $http, $location, $route, $routeParams) {
    if(localStorage.user_gallery_token === undefined){
        $location.url("/sign_in");
    }
    var image_id=$routeParams.image_id;
    
    if(image_id === undefined){
        $scope.gallery_image_info={
            image_title:undefined,
            image_tags:undefined,
            image_desc:undefined,
            image:undefined,
            image_id:0,
        }
    }else{
        $http.get(app_url+"load_image_detail/"+image_id).success(function(response) {
            var text = "";
            for (i = 0; i < response.image_tags.length; i++) {
                var separated='';
                if(i != 0){
                    separated=',';
                }
                
                text +=  separated+response.image_tags[i].tag;
            }
            $scope.gallery_image_info={
                image_title:response.image_detail[0].title,
                image_tags:text,
                image_desc:response.image_detail[0].description,
                image:response.image_detail[0].image,
                image_id:image_id,
            }
        });
    }
    
    // function
    
    $scope.updateGallery = function(valid){
        if(valid){
            var formdata = new FormData();

            formdata.append( 'token', localStorage.user_gallery_token );
            formdata.append( 'image_title', $scope.gallery_image_info.image_title );
            formdata.append( 'image_tags', $scope.gallery_image_info.image_tags );
            formdata.append( 'image_desc', $scope.gallery_image_info.image_desc );
            formdata.append( 'image', $scope.gallery_image_info.image );
            formdata.append( 'image_id', $scope.gallery_image_info.image_id );


            $http.post(app_url+'update_gallery',formdata,
            {
                headers: {'Content-Type' : undefined}
            })
            .success(function(response){
                if(response.status === 'success'){
                    $scope.imageuploadstatus=true;
                }
            })
        }else{
            console.log("invalid form");
        }
    }
    
    $scope.loadUserDetail = function(){
        $scope.user_info=[];
        if(localStorage.user_gallery_token !== undefined){
            var token=localStorage.user_gallery_token;

            $http.get(app_url+"load_user_info/"+token).success(function(response) {
                $scope.user_info = response;
                
                if(response[0].avatar == null || response[0].avatar == ''){
                    $scope.user_avatar=app_gallery_url+"default_user_img.jpg";
                }else{
                    $scope.user_avatar=app_gallery_url+response[0].avatar;
                }
            });
        }
    }
    
    $scope.init=function(){
    if($route.current.method != undefined){
        $scope[$route.current.method]();
    }
}
    
    $scope.init();
});