app.controller('changePassword', function($scope, $http, $location, $route) {
    if(localStorage.user_gallery_token === undefined){
        $location.url("/sign_in");
    }
    $scope.password_info={
        user_password:undefined,
        user_new_password:undefined,
        confirm_password:undefined
    }
    // function
    
    $scope.doChangePassword = function(valid){
        if(valid){
            if($scope.password_info.user_password == $scope.password_info.confirm_password){
                $http({
                    url:app_url+"change_password",
                    method:"POST",
                    headers : { 'Content-Type': 'application/x-www-form-urlencoded' },
                    data: "userpassword=" + $scope.password_info.user_password + "&usernewpassword=" + $scope.password_info.user_new_password + "&token=" + localStorage.user_gallery_token,
                }).success(function (response) {
                    if(response.status === 'password_changed'){
                        $scope.passwordchanged=true;
                    }else if(response.status === 'wrong_password'){
                        $scope.wrongpassword=true;
                    }
                });                
            }else{
                $scope.didnotmatch=true;
            }
 
        }else{
            console.log("invalid form");
        }
    }
    
    $scope.loadUserDetail = function(){
        $scope.user_info=[];
        if(localStorage.user_gallery_token !== undefined){
            var token=localStorage.user_gallery_token;

            $http.get(app_url+"load_user_info/"+token).success(function(response) {
                $scope.user_info = response;
                
                if(response[0].avatar == null || response[0].avatar == ''){
                    $scope.user_avatar=app_gallery_url+"default_user_img.jpg";
                }else{
                    $scope.user_avatar=app_gallery_url+response[0].avatar;
                }
            });
        }
    }
    
    $scope.init=function(){
    if($route.current.method != undefined){
        $scope[$route.current.method]();
    }
}
    
    $scope.init();
});